public class Point {
    int xCoordinate=1;
    int yCoordinate=1;
    public Point(int x,int y){
        xCoordinate=x;
        yCoordinate=y;
    }
    public double distance(Point p){
        return Math.sqrt((xCoordinate-p.xCoordinate)*(xCoordinate-p.xCoordinate)
                +(yCoordinate-p.yCoordinate)*(yCoordinate-p.yCoordinate));
    }

}
