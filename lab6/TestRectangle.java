public class TestRectangle {
    public static void main(String[] args) {
        Point p=new Point(2,3);
        Rectangle r= new Rectangle(5,6,p);
        //Rectangle r= new Rectangle(5,6,new Point(2,3));
        System.out.println("area="+r.area());
        System.out.println("perimeter="+r.perimeter());
        Point[] corners =r.corners();
        for(int i=0;i<corners.length;i++){
            System.out.println("x="+corners[i].xCoordinate+", y="+corners[i].yCoordinate);
        }

    }
}
